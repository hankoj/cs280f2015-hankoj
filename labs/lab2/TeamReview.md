# Team 3 Review
## Jacob Hanko

+ Luke Smith
	+ Luke was in charge of communicating with the customer to figure out what exactly the requirements were going to be.  Luke then passed over his notes to me and I wrote up a short document for Keegan to implement.  Luke dropped the class, and unfortunately did not get to see this Lab through its completion.

+ Keegan Shudy
	+ Keegan set up the repository on Bitbucket as well as started the slack channel.  Keegan was also in charge of implementing the system that would fill the requirements document.  He also typed up a ReadMe file to work as the tutorial for our system.

+ Alex Means
	+ Alex filled me in on everything, because I missed the laboratory session due to an away soccer match.  Alex also was in charge of drawing the system's design diagram.  I know he worked hard on this diagram because he drew it several times by hand before finally making a final draft to have on the computer.

+ Jacob Hanko (myself)
	+ After getting filled in, I was told that I was going to be in charge of documentation as well as being the maintainer.  I had to create the requirements document from the handwritten notes that were given to me by Luke.  I also organized the folders in the repo, and filled out the document that documented our roles within our group.

+ All in all, I think that our group worked well together.  We met in person to go over everything and double check to make sure we had all of the requirements covered.  Everyone in the group was easy to get a hold of if need be.	

