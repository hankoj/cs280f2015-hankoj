# Seniors for 2014-2015

+ _Ryan Cambier_     - request submitted
+ _Tristan Challener_ - no request submitted
+ _Mackenzie Jordan_  - no request submitted
+ _Kara King_         - request submitted
+ _Michael Ligouri_   - request submitted
+ _Alden Page_        - no request submitted
+ _Marlee Sherretts_  - request submitted
+ _Willem Yarbrough_  - no request submitted

# First and Second Reader Assignment

+ _Cambier_   - **Kapfhammer**, **Jumadinova**
+ _Challener_ - **Kapfhammer**, **Wenksovitch**
+ _Jordan_    - **Kapfhammer**, **Jumadinova**
+ _King_      - **Jumadinova**, **Kapfhammer**
+ _Ligouri_   - **Jumadinova**, **Wenksovitch**
+ _Page_      - **Jumadinova**, **Kapfhammer**
+ _Sherretts_ - **Kapfhammer**, **Jumadinova**
+ _Yarbrough_ - **Jumadinova**, **Kapfhammer**

# First Reader Breakdown

+ **Jumadinova**  - King, Ligouri, Page, Yarbrough
+ **Kapfhammer**  - Cambier, Challener, Sherretts
+ **Roos**        - None, on sabbatical
+ **Wenksovitch** - None, visiting assistant professor

# Second Reader Breakdown

+ **Jumadinova**  - Cambier, Jordan, Sherretts
+ **Kapfhammer**  - King, Page, Yarbrough
+ **Roos**        - None, on sabbatical
+ **Wenksovitch** - Challener, Ligouri


# My addition

## Just practicing with different size headers

### This is a header with a size of 3 

